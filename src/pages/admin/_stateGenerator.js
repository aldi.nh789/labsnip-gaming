import { ref } from 'vue';

export function generateCommonDialogState(data, store) {
  const defaults = {};
  const inputDialog = ref(false);
  const deleteDialog = ref(false);
  for (const key in data.value) {
    defaults[key] = data.value[key];
  }
  const setData = (row) => {
    for (const key in data.value) {
      data.value[key] = row ? row[key] : defaults[key] || '';
    }
  };
  return {
    inputDialog,
    deleteDialog,
    openInputDialog(row) {
      setData(row);
      inputDialog.value = true;
    },
    openDeleteDialog(row) {
      setData(row);
      deleteDialog.value = true;
    },
    saveData() {
      if (data.value.id) {
        store.updateItem(data.value.id, data.value);
      } else {
        store.createItem(data.value);
      }
    },
    deleteItem() {
      store.deleteItem(data.value.id);
    },
  };
}

import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/IndexPage.vue') },
      {
        path: 'game',
        children: [
          {
            path: '',
            component: () => import('pages/GamesPage.vue'),
          },
          {
            path: ':slug',
            component: () => import('pages/GameInfoPage.vue'),
          },
        ],
      },
      {
        path: 'blog',
        children: [
          {
            path: '',
            component: () => import('pages/BlogPage.vue'),
          },
          {
            path: ':slug',
            component: () => import('pages/BlogInfoPage.vue'),
          },
        ],
      },
      { path: 'contact', component: () => import('pages/ContactPage.vue') },
    ],
  },
  {
    path: '/admin',
    component: () => import('layouts/AdminLayout.vue'),
    children: [
      {
        path: 'dashboard',
        component: () => import('src/pages/admin/DashboardPage.vue'),
      },
      {
        path: 'user',
        component: () => import('src/pages/admin/UserAdminPage.vue'),
      },
      {
        path: 'game',
        component: () => import('src/pages/admin/GameAdminPage.vue'),
      },
      {
        path: 'blog',
        component: () => import('src/pages/admin/BlogAdminPage.vue'),
      },
      {
        path: 'category',
        component: () => import('src/pages/admin/CategoryAdminPage.vue'),
      },
      {
        path: 'genres',
        component: () => import('src/pages/admin/GenreAdminPage.vue'),
      },
      {
        path: 'tags',
        component: () => import('src/pages/admin/TagAdminPage.vue'),
      },
    ],
  },
  {
    path: '/login',
    component: () => import('src/pages/admin/LoginPage.vue'),
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;

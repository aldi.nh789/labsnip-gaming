import { defineStore } from 'pinia';
import { localAPI } from 'src/boot/axios';

export function createItemStore<T>(name: string, url: string) {
  return defineStore(name, {
    state: () => ({
      items: [] as T[],
    }),
    actions: {
      async fetchItems() {
        try {
          const res = await localAPI.get(url);
          this.items = res.data.payload;
        } catch (err) {
          console.error(err);
        }
      },
      async createItem(item: T) {
        try {
          const res = await localAPI.post(url, item);
          const { meta } = res.data;
          if (meta?.status === 'success') {
            await this.fetchItems();
          } else {
            console.error(meta?.errorMessage);
          }
        } catch (err) {
          console.error(err);
        }
      },
      async updateItem(id: number, item: T) {
        try {
          const res = await localAPI.put(url + '/' + id, item);
          const { meta } = res.data;
          if (meta?.status === 'success') {
            await this.fetchItems();
          } else {
            console.error(meta?.errorMessage);
          }
        } catch (err) {
          console.error(err);
        }
      },
      async deleteItem(id: number) {
        try {
          const res = await localAPI.delete(url + '/' + id);
          const { meta } = res.data;
          if (meta?.status === 'success') {
            await this.fetchItems();
          } else {
            console.error(meta?.errorMessage);
          }
        } catch (err) {
          console.error(err);
        }
      },
    },
  });
}

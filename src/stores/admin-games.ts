import { Game } from './meta';
import { createFilteredStore } from './_store-page-generator';

export const useAdminGamesStore = createFilteredStore<Game>(
  'admin_games',
  '/games'
);

import { defineStore } from 'pinia';
import { localAPI } from 'src/boot/axios';

export function createFilteredStore<T>(name: string, url: string) {
  return defineStore(name, {
    state: () => ({
      items: [] as T[],
      search: '',
      limit: 6,
      page: 1,
      count: 0,
    }),
    actions: {
      async fetchItems() {
        try {
          const searchQuery = `?s=${this.search}`;
          const pageQuery = `&l=${this.limit}&p=${this.page}`;
          const countRes = await localAPI.get(url + '/count' + searchQuery);
          const res = await localAPI.get(url + '/' + searchQuery + pageQuery);
          this.count = countRes.data.payload?.count || 0;
          this.items = res.data.payload;
        } catch (err) {
          console.error(err);
        }
      },
      async createItem(item: T) {
        try {
          const res = await localAPI.post(url, item);
          const { meta } = res.data;
          if (meta?.status === 'success') {
            await this.fetchItems();
          } else {
            console.error(meta?.errorMessage);
          }
        } catch (err) {
          console.error(err);
        }
      },
      async updateItem(id: number, item: T) {
        try {
          const res = await localAPI.put(url + '/' + id, item);
          const { meta } = res.data;
          if (meta?.status === 'success') {
            await this.fetchItems();
          } else {
            console.error(meta?.errorMessage);
          }
        } catch (err) {
          console.error(err);
        }
      },
      async deleteItem(id: number) {
        try {
          const res = await localAPI.delete(url + '/' + id);
          const { meta } = res.data;
          if (meta?.status === 'success') {
            await this.fetchItems();
          } else {
            console.error(meta?.errorMessage);
          }
        } catch (err) {
          console.error(err);
        }
      },
    },
  });
}

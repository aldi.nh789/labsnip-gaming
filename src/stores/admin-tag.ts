import { Tag } from './meta';
import { createItemStore } from './_store-item-generator';

export const useAdminTagsStore = createItemStore<Tag>('admin_tags', '/tags');

import { defineStore } from 'pinia';
import { localAPI } from 'src/boot/axios';
import { Game } from './meta';

export const useInfoGameStore = defineStore('info_games', {
  state: () => ({
    item: {} as Game,
  }),
  actions: {
    async fetchItem(slug: string) {
      try {
        const res = await localAPI.get('/games/' + slug);
        if (res.data.meta?.status === 'success') {
          this.item = res.data.payload;
        } else {
          console.error(res.data.meta?.errorMessage);
        }
      } catch (err) {
        console.error(err);
      }
    },
  },
});

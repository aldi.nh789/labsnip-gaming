import { defineStore } from 'pinia';
import { localAPI } from 'src/boot/axios';
import { Blog, Filter } from './meta';

export const useHomeBlogStore = defineStore('home_blogs', {
  state: () => ({
    items: [] as Blog[],
    search: '',
    limit: 6,
    page: 1,
    count: 0,
    category_id: 0,
    tag_id: 0,
    categories: [] as Filter[],
    tags: [] as Filter[],
  }),
  actions: {
    async fetchFilters() {
      try {
        const resCats = await localAPI.get('/categories/count/all');
        const resTags = await localAPI.get('/tags/count/all');
        if (resCats.data.meta?.status === 'success') {
          this.categories = resCats.data.payload;
        } else {
          console.error(resCats.data?.meta?.errorMessage);
        }
        if (resTags.data.meta?.status === 'success') {
          this.tags = resTags.data.payload;
        } else {
          console.error(resTags.data?.meta?.errorMessage);
        }
      } catch (err) {
        console.log(err);
      }
    },
    async fetchItems() {
      try {
        const url = '/blogs';
        const searchQuery = `?s=${this.search}&tag=${this.tag_id}`;
        const pageQuery = `&l=${this.limit}&p=${this.page}`;
        const resCount = await localAPI.get(
          url + '/filter/count/' + this.category_id + searchQuery
        );
        if (resCount.data.meta?.status === 'success') {
          this.count = resCount.data.payload?.count;
        } else {
          console.error(resCount.data?.meta?.errorMessage);
        }
        const res = await localAPI.get(
          url + '/filter/' + this.category_id + searchQuery + pageQuery
        );
        if (res.data.meta?.status === 'success') {
          this.items = res.data.payload;
        } else {
          console.error(res.data.meta?.errorMessage);
        }
      } catch (err) {
        console.error(err);
      }
    },
    setCategory(category: number) {
      this.category_id = category;
      this.fetchItems();
    },
    setTag(tag: number) {
      this.tag_id = tag;
      this.fetchItems();
    },
  },
});

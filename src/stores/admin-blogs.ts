import { Blog } from './meta';
import { createFilteredStore } from './_store-page-generator';

export const useAdminBlogsStore = createFilteredStore<Blog>(
  'admin_blogs',
  '/blogs'
);

import { defineStore } from 'pinia';
import { localAPI } from 'src/boot/axios';

async function getCount(url: string) {
  const res = await localAPI.get(url + '/count');
  return res.data?.payload?.count || 0;
}

export const useAdminDashboardStore = defineStore('admin_dashboard', {
  state: () => ({
    blogsCount: 0,
    gamesCount: 0,
    categoriesCount: 0,
    tagsCount: 0,
    genresCount: 0,
  }),
  actions: {
    async fetchItems() {
      try {
        this.blogsCount = await getCount('blogs');
        this.gamesCount = await getCount('games');
        this.categoriesCount = await getCount('categories');
        this.genresCount = await getCount('genres');
        this.tagsCount = await getCount('tags');
      } catch (err) {
        console.error(err);
      }
    },
  },
});

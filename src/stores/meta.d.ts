export interface Post {
  id: number;
  title: string;
  slug: string;
  thumbnail: string;
  description: string;
  content: string;
  author: string;
  user_id: string;
  status: string;
  created_at: string;
  updated_at: string;
}

export interface Blog extends Post {
  category: string;
  tags: string[];
}

export interface Game extends Post {
  genre: string;
}

export interface User {
  id: number;
  username: string;
  slug: string;
  phone: string;
  email: string;
  role: string;
  avatar: string;
}

export interface Tag {
  id: number;
  name: string;
}

export interface Filter extends Tag {
  count: number;
}

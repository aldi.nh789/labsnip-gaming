import { defineStore } from 'pinia';
import { localAPI } from 'src/boot/axios';

interface Post {
  id: string;
  title: string;
  slug: string;
  thumbnail: string;
  description: string;
  content: string;
  status: string;
  author: string;
  created_at: string;
  updated_at: string;
}

interface Game extends Post {
  genre: string;
}

interface Blog extends Post {
  category: string;
  tags: string[];
}

export const useLatestNewsStore = defineStore('latests', {
  state: () => ({
    blogs: [] as Blog[],
    games: [] as Game[],
  }),
  actions: {
    async fetchItems() {
      try {
        const blogsRes = await localAPI.get('/blogs?l=3');
        const gamesRes = await localAPI.get('/games?l=3');
        this.blogs = blogsRes.data.payload;
        this.games = gamesRes.data.payload;
      } catch (err) {
        console.error(err);
      }
    },
  },
});

import { defineStore } from 'pinia';
import { localAPI } from 'src/boot/axios';
import { Game, Filter } from './meta';

export const useHomeBlogStore = defineStore('home_games', {
  state: () => ({
    items: [] as Game[],
    search: '',
    limit: 6,
    page: 1,
    count: 0,
    genre_id: 0,
    genres: [] as Filter[],
  }),
  actions: {
    async fetchFilters() {
      try {
        const resGens = await localAPI.get('/genres/count/all');
        if (resGens.data.meta?.status === 'success') {
          this.genres = resGens.data.payload;
        } else {
          console.error(resGens.data?.meta?.errorMessage);
        }
      } catch (err) {
        console.log(err);
      }
    },
    async fetchItems() {
      try {
        const url = '/games';
        const searchQuery = `?s=${this.search}`;
        const pageQuery = `&l=${this.limit}&p=${this.page}`;
        const resCount = await localAPI.get(
          url + '/filter/count/' + this.genre_id + searchQuery
        );
        if (resCount.data.meta?.status === 'success') {
          this.count = resCount.data.payload?.count;
        } else {
          console.error(resCount.data?.meta?.errorMessage);
        }
        const res = await localAPI.get(
          url + '/filter/' + this.genre_id + searchQuery + pageQuery
        );
        if (res.data.meta?.status === 'success') {
          this.items = res.data.payload;
        } else {
          console.error(res.data.meta?.errorMessage);
        }
      } catch (err) {
        console.error(err);
      }
    },
    setGenre(category: number) {
      this.genre_id = category;
      this.fetchItems();
    },
  },
});

import { Tag } from './meta';
import { createItemStore } from './_store-item-generator';

export const useAdminGenresStore = createItemStore<Tag>(
  'admin_genres',
  '/genres'
);

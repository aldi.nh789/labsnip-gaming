import { Tag } from './meta';
import { createItemStore } from './_store-item-generator';

export const useAdminCategoriesStore = createItemStore<Tag>(
  'admin_categories',
  '/categories'
);

import { defineStore } from 'pinia';
import { localAPI } from 'src/boot/axios';
import { Blog } from './meta';

export const useInfoBlogStore = defineStore('info_blogs', {
  state: () => ({
    item: {} as Blog,
  }),
  actions: {
    async fetchItem(slug: string) {
      try {
        const res = await localAPI.get('/blogs/' + slug);
        if (res.data.meta?.status === 'success') {
          this.item = res.data.payload;
        } else {
          console.error(res.data.meta?.errorMessage);
        }
      } catch (err) {
        console.error(err);
      }
    },
  },
});

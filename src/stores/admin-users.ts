import { User } from './meta';
import { createItemStore } from './_store-item-generator';

export const useAdminUsersStore = createItemStore<User>(
  'admin_users',
  '/users'
);

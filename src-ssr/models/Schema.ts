import db from './db';

interface Structure {
  string?: string[];
  text?: string[];
  ref?: [column: string, refTable: string][];
}

/**
 * Usage :
 *
 * new Schema('tableName', [
 *  string: ['column'],
 *  text: ['column'],
 *  ref: [['column', 'refTable']]
 * ])
 */
export default class Schema {
  tableName: string;
  structure: Structure;

  constructor(tableName: string, structure: Structure, init?: () => any) {
    this.tableName = tableName;
    this.structure = structure;
    db.schema
      .hasTable(tableName)
      .then((exists) => {
        if (!exists) {
          return db.schema.createTable(tableName, (tbuilder) => {
            tbuilder.increments();
            for (const column of structure.string || []) {
              tbuilder.string(column);
            }
            for (const column of structure.text || []) {
              tbuilder.text(column);
            }
            for (const [column, refTable] of structure.ref || []) {
              tbuilder
                .integer(column)
                .unsigned()
                .references('id')
                .inTable(refTable);
            }
            tbuilder
              .dateTime('created_at')
              .notNullable()
              .defaultTo(db.raw('CURRENT_TIMESTAMP'));

            tbuilder
              .dateTime('updated_at')
              .notNullable()
              .defaultTo(
                db.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
              );
          });
        }
      })
      .then(() => {
        init && init();
      });
  }

  async findOrCreateWhere(param: any, value: any) {
    const row = await this.where(param, value).first();
    if (row) {
      return row.id;
    } else {
      const [id] = await this.insert({ [param]: value });
      return id;
    }
  }

  column(columnName: string, as?: string) {
    return this.tableName + '.' + columnName + (as ? ' as ' + as : '');
  }

  all() {
    return db(this.tableName);
  }

  where(param: any, value: any) {
    return this.all().where(`${this.tableName}.${param}`, value);
  }

  get(id: number) {
    return this.where('id', id).first();
  }

  allJoin() {
    let query = this.all();
    for (const [column, refTable] of this.structure.ref || []) {
      query = query.join(refTable, {
        [this.column(column)]: `${refTable}.id`,
      });
    }
    return query;
  }

  insert(data: any) {
    return this.all().insert(data);
  }

  update(id: number, data: any) {
    return this.where('id', id).update(data);
  }

  delete(id: number) {
    return this.where('id', id).del();
  }

  static createSchema(
    tableName: string,
    structure: Structure,
    init?: () => any
  ): Schema {
    return new Schema(tableName, structure, init);
  }
}

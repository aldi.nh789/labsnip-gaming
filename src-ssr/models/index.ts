import { hash } from '../api/user';
import Schema from './Schema';

// User
export const User = Schema.createSchema(
  'users',
  {
    string: [
      'username',
      'password',
      'email',
      'slug',
      'role',
      'phone',
      'avatar',
    ],
  },
  () => {
    // Create admin if not exists
    User.where('role', 'admin')
      .first()
      .then((admin) => {
        if (!admin) {
          User.insert({
            username: 'admin',
            email: 'admin@gmail.com',
            password: hash('admin123'),
            slug: 'admin',
            role: 'admin',
          }).then();
        }
      });
  }
);

// Game > Genre
export const Genre = Schema.createSchema('genres', {
  string: ['name'],
});

export const Game = Schema.createSchema('games', {
  string: ['title', 'slug', 'thumbnail', 'description', 'status'],
  text: ['content'],
  ref: [
    ['genre_id', 'genres'],
    ['user_id', 'users'],
  ],
});

// Post > Category
// Post > PostTag < Tags
export const Category = Schema.createSchema('categories', {
  string: ['name'],
});

export const Tag = Schema.createSchema('tags', {
  string: ['name'],
});

export const Post = Schema.createSchema('posts', {
  string: ['title', 'slug', 'thumbnail', 'description', 'status'],
  text: ['content'],
  ref: [
    ['category_id', 'categories'],
    ['user_id', 'users'],
  ],
});

export const PostTag = Schema.createSchema('posts_tags', {
  ref: [
    ['post_id', 'posts'],
    ['tag_id', 'tags'],
  ],
});

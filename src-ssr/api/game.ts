import { createMeta } from '../boot/meta';
import { crud, formatRowDate, pageAndFilterQuery } from '../boot/rest';
import { slugify } from '../boot/slugify';
import { Game, Genre, User } from '../models';

const indexColumns = ['title', 'description', 'content'];
const selects = [
  Game.column('id'),
  Game.column('title'),
  Game.column('slug'),
  Game.column('thumbnail'),
  Game.column('description'),
  Game.column('content'),
  Game.column('status'),
  Genre.column('name', 'genre'),
  User.column('username', 'author'),
  Game.column('created_at'),
  Game.column('updated_at'),
];

const router = crud(Game, {
  readIndex: 'slug',
  searchColumns: ['title', 'description', 'content'],
  dataReader() {
    return Game.allJoin()
      .select(...selects)
      .orderBy(Game.column('created_at'), 'desc');
  },
  async updateModifier(data, prev) {
    const genre_id = await Genre.findOrCreateWhere('name', data.genre);
    const title: string = data.title || prev?.title || '';
    return {
      title: title,
      slug: prev?.slug || (await slugify(Game, title)),
      thumbnail: data.thumbnail || prev?.thumbnail || '',
      description: data.description || prev?.description || '',
      content: data.content || prev?.content || '',
      status: data.status || prev?.status || 'draft',
      genre_id: genre_id || prev?.genre_id || 0,
      user_id: data.user_id || prev?.user_id || 0,
    };
  },
  preRoute(router) {
    router.get('/filter/count/:genre_id', async (req, res) => {
      res.json(
        await createMeta(async () => {
          const genre_id = req.params.genre_id;
          let query = Game.allJoin()
            .select(...selects)
            .orderBy(Game.column('created_at'), 'desc');
          if (genre_id && genre_id !== '0') {
            query = query.where(Game.column('genre_id'), genre_id);
          }
          const [row] = await pageAndFilterQuery(
            query,
            req,
            indexColumns,
            Game
          ).count();
          return { count: row['count(*)'] };
        })
      );
    });
    router.get('/filter/:genre_id', async (req, res) => {
      res.json(
        await createMeta(async () => {
          const genre_id = req.params.genre_id;
          let query = Game.allJoin()
            .select(...selects)
            .orderBy(Game.column('created_at'), 'desc');
          if (genre_id && genre_id !== '0') {
            query = query.where(Game.column('genre_id'), genre_id);
          }
          const out = await pageAndFilterQuery(query, req, indexColumns, Game);
          return formatRowDate(out);
        })
      );
    });
  },
});

export default router;

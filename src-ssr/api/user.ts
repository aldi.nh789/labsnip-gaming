import { createHash } from 'crypto';
import { crud } from '../boot/rest';
import { slugify } from '../boot/slugify';
import { User } from '../models';

export function hash(plain: string): string {
  return createHash('sha256').update(plain).digest('base64');
}

const router = crud(User, {
  dataReader() {
    return User.all().select(
      User.column('id'),
      User.column('username'),
      User.column('slug'),
      User.column('phone'),
      User.column('email'),
      User.column('role'),
      User.column('avatar'),
      User.column('created_at'),
      User.column('updated_at')
    );
  },
  async updateModifier(data, prev) {
    const username = data.username || prev?.username || '';
    return {
      username: username,
      password: prev?.password || hash(data.password),
      slug: prev?.slug || (await slugify(User, username)),
      phone: data.phone || prev?.phone || '',
      email: data.email || prev?.email || '',
      role: data.role || prev?.role || '',
      avatar: data.avatar || prev?.avatar || '',
    };
  },
});

export default router;

import { Router, json } from 'express';
import apiUser from './user';
import apiGame from './game';
import apiPost from './post';
import apiAuth from './auth';
import apiCategory from './category';
import apiTag from './tag';
import apiGenre from './genre';

const router = Router();

router.use(json());
router.use('/users', apiUser);
router.use('/games', apiGame);
router.use('/blogs', apiPost);
router.use('/tags', apiTag);
router.use('/categories', apiCategory);
router.use('/genres', apiGenre);
router.use('/auth', apiAuth);

export default router;

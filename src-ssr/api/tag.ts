import { createMeta } from '../boot/meta';
import { crud } from '../boot/rest';
import { PostTag, Tag } from '../models';

const router = crud(Tag, {
  async beforeDelete(id) {
    await PostTag.where('tag_id', id).del();
  },
  preRoute(router) {
    router.get('/count/all', async (_req, res) => {
      res.json(
        await createMeta(() => {
          return PostTag.allJoin()
            .groupBy(Tag.column('name'))
            .select(Tag.column('id'), Tag.column('name'))
            .count(Tag.column('id', 'count'));
        })
      );
    });
  },
});

export default router;

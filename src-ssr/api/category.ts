import { createMeta } from '../boot/meta';
import { crud } from '../boot/rest';
import { Category, Post } from '../models';

const router = crud(Category, {
  preRoute(router) {
    router.get('/count/all', async (_req, res) => {
      res.json(
        await createMeta(() => {
          return Post.allJoin()
            .groupBy(Category.column('name'))
            .select(Category.column('id'), Category.column('name'))
            .count(Category.column('id', 'count'));
        })
      );
    });
  },
});

export default router;

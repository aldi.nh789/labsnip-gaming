import { createMeta } from '../boot/meta';
import { crud, formatRowDate, pageAndFilterQuery } from '../boot/rest';
import { slugify } from '../boot/slugify';
import { Category, Post, PostTag, Tag, User } from '../models';

const indexColumns = ['title', 'description', 'content'];
const selects = [
  Post.column('id'),
  Post.column('title'),
  Post.column('slug'),
  Post.column('thumbnail'),
  Post.column('description'),
  Post.column('content'),
  Post.column('status'),
  Category.column('name', 'category'),
  User.column('username', 'author'),
  Post.column('created_at'),
  Post.column('updated_at'),
];

const router = crud(Post, {
  readIndex: 'slug',
  searchColumns: indexColumns,
  dataReader() {
    return Post.allJoin()
      .select(...selects)
      .orderBy(Post.column('created_at'), 'desc');
  },
  async rowModifier(row) {
    const tags = await PostTag.allJoin()
      .where('post_id', row.id)
      .select(Tag.column('name', 'tagname'));
    row.tags = tags.map((tag) => tag.tagname);
    return row;
  },
  async updateModifier(data, prev) {
    const category_id = await Category.findOrCreateWhere('name', data.category);
    const title: string = data.title || prev?.title || '';
    return {
      title: title,
      slug: prev?.slug || (await slugify(Post, title)),
      thumbnail: data.thumbnail || prev?.thumbnail || '',
      description: data.description || prev?.description || '',
      content: data.content || prev?.content || '',
      status: data.status || prev?.status || 'draft',
      category_id: category_id || prev?.category || 0,
      user_id: data.user_id || prev?.user_id || 0,
    };
  },
  async afterUpdate(data, id) {
    await PostTag.where('post_id', id).del();
    if (id) {
      for (const tag of data.tags || []) {
        const tagId = await Tag.findOrCreateWhere('name', tag);
        await PostTag.insert({ post_id: id, tag_id: tagId });
      }
    }
  },
  async beforeDelete(id) {
    await PostTag.where('post_id', id).del();
  },
  preRoute(router) {
    router.get('/filter/count/:category_id', async (req, res) => {
      res.json(
        await createMeta(async () => {
          const category_id = req.params.category_id;
          const tag_id = req.query.tag as string;
          const query = filterBlog(category_id, tag_id);
          const [row] = await pageAndFilterQuery(
            query,
            req,
            indexColumns,
            Post
          ).count();
          return { count: row['count(*)'] };
        })
      );
    });
    router.get('/filter/:category_id', async (req, res) => {
      res.json(
        await createMeta(async () => {
          const category_id = req.params.category_id;
          const tag_id = req.query.tag as string;
          const query = filterBlog(category_id, tag_id);
          const out = await pageAndFilterQuery(query, req, indexColumns, Post);
          return formatRowDate(out);
        })
      );
    });
  },
});

function filterBlog(category_id: string, tag_id: string) {
  let query = Post.allJoin();
  if (tag_id && tag_id !== '0') {
    query = PostTag.allJoin()
      .join(User.tableName, {
        [Post.column('user_id')]: User.column('id'),
      })
      .join(Category.tableName, {
        [Post.column('category_id')]: Category.column('id'),
      })
      .where(PostTag.column('tag_id'), tag_id);
  }
  if (category_id && category_id !== '0') {
    query = query.where(Post.column('category_id'), category_id);
  }
  return query.select(...selects).orderBy(Post.column('created_at'), 'desc');
}

export default router;

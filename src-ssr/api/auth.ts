import { Router } from 'express';
import { createMeta } from '../boot/meta';
import { User } from '../models';
import { hash } from './user';

const router = Router();

export const userTokens = new Map();

function createRandomToken() {
  let token = '';
  for (let i = 0; i < 16; i++) {
    token += Math.floor(Math.random() * 36).toString(36);
  }
  return token;
}

/**
 * POST /api/auth/login
 *
 * <header>
 * auth_token: string
 *
 * <body>
 * email: string
 * password: string
 *
 */
router.post('/login', async (req, res) => {
  res.json(
    await createMeta(async () => {
      const authToken = req.headers.auth_token;
      if (authToken) {
        if (userTokens.has(authToken)) {
          const user = userTokens.get(authToken);
          return { auth_token: authToken, user: user };
        } else {
          throw Error('invalid or expired token');
        }
      } else {
        const email = req.body.email || '';
        const password = req.body.password || '';
        const user = await User.where('email', email)
          .andWhere('password', hash(password))
          .select('id', 'username', 'role')
          .first();
        if (user) {
          for (const token of userTokens.keys()) {
            const storedUser = userTokens.get(token);
            if (storedUser.id === user.id) {
              return { auth_token: token, user: user };
            }
          }
          const createdToken = createRandomToken();
          userTokens.set(createdToken, user);
          return { auth_token: createdToken, user: user };
        } else {
          throw Error('email or password invalid');
        }
      }
    })
  );
});

/**
 * POST /api/auth/logout
 *
 * <header>
 * auth_token: string
 *
 */
router.post('/logout', async (req, res) => {
  res.json(
    await createMeta(async () => {
      const authToken = req.headers.auth_token;
      if (authToken) {
        userTokens.delete(authToken);
      }
    })
  );
});

export default router;

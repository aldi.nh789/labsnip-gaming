import { createMeta } from '../boot/meta';
import { crud } from '../boot/rest';
import { Genre, Game } from '../models';

const router = crud(Genre, {
  preRoute(router) {
    router.get('/count/all', async (_req, res) => {
      res.json(
        await createMeta(() => {
          return Game.allJoin()
            .groupBy(Genre.column('name'))
            .select(Genre.column('id'), Genre.column('name'))
            .count(Genre.column('id', 'count'));
        })
      );
    });
  },
});

export default router;

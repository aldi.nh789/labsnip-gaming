interface MetaHeader {
  status: 'success' | 'error';
  errorMessage?: string;
  inserted?: number;
  updated?: number;
  deleted?: number;
}

interface Meta {
  meta: MetaHeader;
  payload?: any;
}

export async function createMeta(
  prom: (header: MetaHeader) => Promise<any>
): Promise<Meta> {
  try {
    const metaHeader: MetaHeader = {
      status: 'success',
    };
    const payload = await prom(metaHeader);
    return {
      meta: metaHeader,
      payload: payload,
    };
  } catch (err) {
    if (err instanceof Error) {
      return {
        meta: {
          status: 'error',
          errorMessage: err.message,
        },
      };
    } else {
      return {
        meta: {
          status: 'error',
          errorMessage: JSON.stringify(err),
        },
      };
    }
  }
}

import { Router, Request } from 'express';
import { Knex } from 'knex';
import Schema from '../models/Schema';
import { createMeta } from './meta';

function simplifyQuery(query: any) {
  if (query instanceof Array) {
    return query[0].toString();
  } else if (typeof query === 'string') {
    return query;
  } else {
    return '';
  }
}

export function pageAndFilterQuery(
  result: Knex.QueryBuilder,
  req: Request,
  searchColumns: string[],
  schema: Schema
): Knex.QueryBuilder {
  // Search Query
  const s = simplifyQuery(req.query.s);
  // Display Limit
  const l = simplifyQuery(req.query.l);
  // Pagination, must have limit
  const p = simplifyQuery(req.query.p);
  if (s && searchColumns.length > 0) {
    result = result.andWhere((qb) => {
      qb = qb.whereLike(schema.column(searchColumns[0]), '%' + s + '%');
      for (let i = 1; i < searchColumns.length; i++) {
        qb.orWhereLike(schema.column(searchColumns[i]), '%' + s + '%');
      }
    });
  }
  const limit = parseInt(l);
  if (limit) {
    result = result.limit(limit);
    const page = parseInt(p);
    if (page) {
      const offset = (page - 1) * limit;
      result.offset(offset);
    }
  }
  return result;
}

export function formatRowDate(row: any) {
  if (row.created_at) {
    row.created_at = Intl.DateTimeFormat(row.created_at).format();
  }
  if (row.updated_at) {
    row.updated_at = Intl.DateTimeFormat(row.updated_at).format();
  }
  return row;
}

export function crud(
  schema: Schema,
  options: {
    readIndex?: string;
    searchColumns?: string[];
    dataReader?(): Knex.QueryBuilder;
    rowModifier?(row: any): any;
    updateModifier?(data: any, prev?: any): any;
    afterUpdate?(data: any, id?: number): any;
    beforeDelete?(id: number): any;
    preRoute?(router: Router): any;
  } = {}
) {
  const readIndex = options.readIndex || 'id';
  const searchColumns = options.searchColumns || [];
  const dataReader = options.dataReader || (() => schema.all());
  const rowModifier = options.rowModifier;
  const updateModifier = options.updateModifier || ((data) => data);
  const afterUpdate = options.afterUpdate || (() => {});
  const beforeDelete = options.beforeDelete || (() => {});
  const preRoute = options.preRoute || (() => {});
  const router = Router();

  preRoute(router);

  router.get('/count', async (req, res) => {
    res.json(
      await createMeta(async () => {
        const result = pageAndFilterQuery(
          dataReader(),
          req,
          searchColumns,
          schema
        );
        const [row]: any = await result.count();
        return { count: row['count(*)'] };
      })
    );
  });

  router.get('/', async (req, res) =>
    res.json(
      await createMeta(async () => {
        const result = pageAndFilterQuery(
          dataReader(),
          req,
          searchColumns,
          schema
        );
        const rows: any[] = await result;
        const out = rowModifier
          ? await Promise.all(rows.map(rowModifier))
          : rows;
        return out.map(formatRowDate);
      })
    )
  );

  router.get('/:index', async (req, res) => {
    res.json(
      await createMeta(async () => {
        const row = await dataReader()
          .where(schema.column(readIndex), req.params.index)
          .first();
        if (row) {
          const out = rowModifier ? await rowModifier(row) : row;
          return formatRowDate(out);
        } else {
          throw Error(
            "data not found with '" + readIndex + "' " + req.params.index
          );
        }
      })
    );
  });

  router.post('/', async (req, res) =>
    res.json(
      await createMeta(async (header) => {
        const data: any = await updateModifier(req.body);
        const [id] = await schema.insert(data);
        header.inserted = 1;
        afterUpdate(req.body, id);
        return { id: id };
      })
    )
  );

  router.put('/:id', async (req, res) =>
    res.json(
      await createMeta(async (header) => {
        const id = parseInt(req.params.id) || 0;
        const row = await schema.get(id);
        if (row) {
          const data = await updateModifier(req.body, row);
          header.updated = await schema.update(id, data);
          afterUpdate(req.body, row.id);
        } else {
          throw Error('data not found');
        }
      })
    )
  );

  router.delete('/:id', async (req, res) => {
    res.json(
      await createMeta(async (header) => {
        const id = parseInt(req.params.id) || 0;
        const row = await schema.get(id);
        if (row) {
          beforeDelete(id);
          header.deleted = await schema.delete(id);
        } else {
          throw Error('data not found');
        }
      })
    );
  });

  return router;
}

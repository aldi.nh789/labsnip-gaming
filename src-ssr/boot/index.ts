import { Express } from 'express';
import router from '../api';

export function setupServer(app: Express) {
  app.use('/api', router);
}

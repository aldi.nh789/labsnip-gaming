import Schema from '../models/Schema';

export async function slugify(schema: Schema, title: string) {
  const initialSlug = title.replaceAll(/\s+/g, '-');
  let availableSlug = initialSlug;
  let diff = 0;
  let posts = await schema.where('slug', availableSlug);
  while (posts.length > 0) {
    diff++;
    availableSlug = initialSlug + '-' + diff;
    posts = await schema.where('slug', availableSlug);
  }
  return availableSlug;
}
